FROM docker.io/elasticsearch:7.8.1

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG TZ
ARG http_proxy
ARG https_proxy
ARG ES_JAVA_OPTS

RUN elasticsearch-plugin install analysis-icu
RUN elasticsearch-plugin install analysis-kuromoji

